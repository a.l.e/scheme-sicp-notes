# Lesson 1

Code examples from the book and the lecture:

- [buzz.rk](buzz.rk)
- [pig-latin.rk](pig-latin.rk)
- [plural.rk](plural.rk)
- [sqrt.rk](sqrt.rk)
- [sqrt-internal.rk](sqrt-internal.rk) (square root with function local to `sqrt`)

## Homeworks

### new-if and sqrt

Exercise 1.6, page 25.

The new-if does not shortcut the second argument and runs both the `then-clause` and the `else-cause`.  
Beacause of this, the recursion will never stop.

For pigl i get the same result.


### Square of multiple numbers

sentence is the solution?

- [squares.rk](squares.rk)

### I told you, you told me

- [switch.rk](switch.rk)

### ordered list?

- [ordered.rk](ordered.rk)

### words ending with e

- [ends-e.rk](ends-e.rk)

First create an or between a function that returns `true` and one that returns `false`:

```scheme
(define (down-to-zero x)
  true
)

(define (up from-zero x)
  false
)

(or (down-to-zero 10) (up-from-zero 0))
```

Then implement the functions so that the first one will return `true` after a finite time and the second will get in an infinte loop:

- [or-special-form.rk](or-special-form.rk)

If `or` is a special form, we will get a result in a finite time, otherwise the program will get in the infinite loop.

# Week 4


## 2.1 A better make rational

[make-better-rational.rk](make-better-rational.rk)

## 2.2 Make segements

[make-segment.rk](make-segment.rk)

## 2.4 Procedural pairs

[procedural-pairs.rk](procedural-pairs.rk)

## 2.5 Numeric pairs

[numeric-pairs.rk](numeric-pairs.rk)

## 2.7, 2.8, 2.10, 2.12  Interval arithmetics

[interval-arithmetic.rk](interval-arithmetic.rk)   

... i'm not sure that `sub-interval` really makes sense...

## 2.17 List operations

[list-operations.rk](list-operations.rk)

## 2.20 Dot arguments

[same-parity.rk](same-parity.rk)

## 2.21, 2.22 Scaling a list

[square-list.rk](square-list.rk)

... ok, i don't 100% get why the iter versions do not work.

i guess it would work with `apply`

## 2.23 For each

[for-each.rk](for-each.rk)

# Structure and Interpretation of Computer Programs

Coders Only, Studying the Wizard Book

<https://git.sr.ht/~codersonly/wizard-book-study>

## Installing drracket

- get the .sh from the drracket website, put it in `~/bin` and execute it to install drracket.

## simply-scheme

We will be using `simply-scheme` a version with some handy function for text processing:

- add `(require (planet dyoo/simply-scheme:2:2))` after `#lang racket` to run the examples from CS 61A as they are
  - There are few more hints in: <https://stackoverflow.com/questions/19203498/adding-simply-scheme-language-to-drracket>
- All examples from the cs61a classes as scm files: <https://www-inst.eecs.berkeley.edu//~instcd/inst-cd/classes/cs61a/lectures/>
- simply-scheme commands list: <https://docs.racket-lang.org/manual@simply-scheme/index.html>

## The book

- The [official html version](https://mitpress.mit.edu/sites/default/files/sicp/index.html)
- The [Simply Scheme](https://people.eecs.berkeley.edu/~bh/ss-toc2.html) book by the same author of our lessons.
- [sarabander](https://github.com/sarabander) has [a nice html version](http://sarabander.github.io/sicp/), but her pdf is not available anymore.  
  It can be found [here](https://opendocs.github.io/sicp/)

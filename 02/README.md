# Lesson 2

Section 1.3

## Using a common sum function

- [sum-general.rk](sum-general.rk)

## Integrals according to the Simpson's rule

Exercise 1.29

partially solved:

- [integral-simpson.rk](integral-simpson.rk)
- how to get it to switch between the 2 and 4 in the multiplications?

## Iterative sum

- [sum-iterative.rk](sum-iterative.rk)

## Product of numbers

- [product.rk](product.rk)

## Return functions

- [average-damp.rk](average-damp.rk)

## Exercises

lab:

- 2. [substitute.rk](substitute.rk)
- 3. `(define (g) (lambda (x) (* x 3)))`  
     `g` does not have any argument and returns a function
- 4. [f.rk](f.rk)
- 5. [t.rk](t.rk)
- 6. [t.rk](t.rk)
- 7. [make-tester.rk](make-tester.rk)

book:

- 1.31: see product.rk
- 1.32: accumulate.rk
- 1.33: filter.rk
- 1.40: cubic.rk (unfinished...)

extra:

- [y-combinator.rk](y-combinator.rk)
